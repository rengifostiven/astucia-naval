<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    </head>
    <body>
        <div class="row content">
            <div class="col-md-12 ">
                <h1 class="text-body mt-4"><strong>Astucia Naval</strong></h1>
                <div id="board"></div>
            </div>
            <div class="col-md-12 col-sm-12 text-center">
                <div class="bd-example form-inline">
                    <label class="my-1 mr-2 font-weight-bold" for="attack_id">¡Ataca a tus enemigos!</label>
                    <input type="text" class="form-control mt-2 mb-2 mr-md-2" id="attack_input" maxlength="3">
                    <button type="button" class="btn btn-success my-1" id="attack_btn">Disparar <i class="fa fa-rocket"></i></button>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 text-center mt-4">
                <span class="text-muted">Desarrollado por: <a href="https://www.linkedin.com/in/stiven-rengifo-b46061128/" target="_blank">Stiven Rengifo</a></span>
             </div>
        </div>
    </body>
</html>

"use strict";

const boardDiv          = document.querySelector("#board");
const numberOfboxes     = 10;
const minRandomNumber   = 1;
const maxRandomNumber   = 11;
const maxEnemy          = 20;
let arrayRows           = ['A','B','C','D','E','F','G','H','I','J'];
let board               = [];

(function() {

    //Creación del tablero
    for(let i = 0; i <= numberOfboxes; i++){
        let row  = document.createElement("div");
        boardDiv.appendChild(row);
        row.className = "row";
        board[i] = [];

        for(let j = 0; j <= numberOfboxes; j++ ){
            let grid = document.createElement("div");
            row.appendChild(grid);
            grid.className = "grid";
            grid.id = i + "-" + j;
            board[i][j] = 0;

            if(i> 0 && j >0){
                grid.classList.add('water');

            }else{
                let span = document.createElement('span');
                grid.appendChild(span);

                if(i > 0 && j === 0){
                    span.textContent = arrayRows[(i-1)];
                }else if(i ===0 && j > 0){
                    span.textContent = j;
                }
            }
        }
    }

    //Marca coordenadas de barcos enemigos aleatoriamente.
    for(let k = 0; k < maxEnemy; k++){
        let rowNumber    = getRandomInt(minRandomNumber, maxRandomNumber);
        let columnNumber = getRandomInt(minRandomNumber, maxRandomNumber);

        markPosition(rowNumber, columnNumber, false);
    }

})();

/**
 * Evento del botón atacar
 */
document.getElementById("attack_btn").addEventListener("click", function() {

    let inputValue  = document.getElementById("attack_input").value.trim();

    if(inputValue == '' || inputValue == null){
        alert('¡Ingrese la coordenada de ataque!');

    }else{

        let row      = arrayRows.indexOf(inputValue.toUpperCase().charAt(0));
        let column   = parseInt(inputValue.substr(1,2).trim());

        try{
            markPosition( (parseInt(row)+1), column, true);
        }catch(error){
            alert('¡Coordenada incorrecta!')
        }
    }

    document.getElementById('attack_input').value = "";

});

/**
 * Permite marcar las posiciones de la matriz
 */
function markPosition(row, column , isAttack) {

    let box = document.getElementById( (row+'-'+column) );

    if(isAttack){
        console.log(box.value);
        if( typeof box.value !== 'undefined'){
            box.innerHTML = '<div class="icon-enemy"><i class="fa fa-ship"></i></div><div class="attack">X</div>';
            box.value = '';
        }else{
            box.innerHTML = '<div class="attack">X</div>';
        }

    }else{
        box.innerHTML = '<div class="icon-enemy"><i class="fa fa-ship"></i><div>';
        box.value = 1;
    }
}

/**
 * Retorna un entero aleatorio entre min (incluido) y max (excluido)
 * @param {*} min
 * @param {*} max
 */
function getRandomInt(min, max) {

    return Math.floor(Math.random() * (max - min)) + min;
}
